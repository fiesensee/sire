import { applyMiddleware, compose, createStore } from 'redux'
import thunkMiddleware from 'redux-thunk'
import monitorReducersEnhancer from './enhancers/monitorReducers'
import loggerMiddleware from './middleware/logger'
import rootReducer from './reducers'
import { persistStore, persistReducer } from 'redux-persist';
import createElectronStorage from "redux-persist-electron-storage";

export default function configureStore(preloadedState) {
    const middlewares = [loggerMiddleware, thunkMiddleware]
    const middlewareEnhancer = applyMiddleware(...middlewares)
    const enhancers = [middlewareEnhancer, monitorReducersEnhancer]
    const composedEnhancers = compose(...enhancers)

    const persistConfig = {
        key: 'root',
        storage: createElectronStorage()
    }
    
    const persistedReducer = persistReducer(persistConfig, rootReducer);

    const store = createStore(persistedReducer, preloadedState, composedEnhancers);
    const persistor = persistStore(store);
    return { store, persistor }
}