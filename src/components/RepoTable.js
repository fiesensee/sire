import React from 'react';
import Container from '@material-ui/core/Container';
import * as gitworker from '../worker/gitworker';
import MaterialTable from "material-table";
import { makeStyles } from '@material-ui/core';
import GetAppIcon from '@material-ui/icons/GetApp';
import PublishIcon from '@material-ui/icons/Publish';
import RefreshIcon from '@material-ui/icons/Refresh';
import { useSelector, useDispatch } from 'react-redux';
import { setRepo, setRepos, setIsLoading } from '../actionTypes';


const useStyles = makeStyles(theme => ({
    list: {
        paddingBottom: 50,
    },
}));

function RepoTable() {
    const classes = useStyles();

    const repos = Object.values(useSelector(state => state.git));
    const path = useSelector(state => state.directory);
    const dispatch = useDispatch();

    function getRepoModel(repo) {
        return {
            name: repo.repoName,
            status: repo.changes.length === 0 ? "Clean" : "Changed",
            commitsBehind: repo.commitsBehind,
            commitsAhead: repo.commitsAhead,
            currentBranch: repo.currentBranch.replace("refs/heads/", ""),
            key: repo.repoName,
            path: repo.path
        }
    }

    function refreshRepos(path) {
        dispatch(setRepos({}));
        dispatch(setIsLoading(true));
        return gitworker.refreshRepos(path).then(repoPromises => {
            return Promise.all(repoPromises.map(repoPromise => {
                return repoPromise.then(repo => {
                    if (repo !== undefined) {
                        dispatch(setRepo(getRepoModel(repo)));
                    }
                })
            }))
        })
    }

    function processRepos(repos, fun) {
        dispatch(setIsLoading(true))
        return Promise.all(repos.map(repo => {
            return fun(repo.path).then((updatedRepo) => {
                dispatch(setRepo(getRepoModel(updatedRepo)));
            });
        }));
    }

    let columns = [
        { title: "Repo Name", field: "name" },
        { title: "Status", field: "status" },
        { title: "Commits behind", field: "commitsBehind" },
        { title: "Commits ahead", field: "commitsAhead" },
        { title: "Current Branch", field: "currentBranch" }
    ]

    return (
        <Container className={classes.list}>
            <MaterialTable
                columns={columns}
                data={repos}
                title="Repos"
                options={{ 
                    paging: false, 
                    actionsColumnIndex: columns.length, 
                    selection: true,
                    maxBodyHeight: '500px',
                    headerStyle: { position: 'sticky', top: 0 },
                 }}
                actions={[
                    {
                        icon: 'refresh',
                        tooltip: 'Refresh repos',
                        isFreeAction: true,
                        onClick: () => {
                            refreshRepos(path).then(result => {
                                dispatch(setIsLoading(false));
                            })
                        }
                    },
                    {
                        icon: () => <RefreshIcon />,
                        tooltip: 'Refresh repo',
                        onClick: (event, repos) => processRepos(repos, gitworker.refreshRepo).then(() => {
                            dispatch(setIsLoading(false));
                        })
                    },
                    {
                        icon: () => <GetAppIcon />,
                        tooltip: 'Pull repo',
                        onClick: (event, repos) => processRepos(repos, gitworker.pullRepo).then(() => {
                            dispatch(setIsLoading(false));
                        })
                    },
                    {
                        icon: () => <PublishIcon />,
                        tooltip: 'Push repo',
                        onClick: (event, repos) => processRepos(repos, gitworker.pushRepo).then(() => {
                            dispatch(setIsLoading(false));
                        })
                    }
                ]}
            />
        </Container>
    );
}

export default RepoTable;