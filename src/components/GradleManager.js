import React from 'react'
import * as gradleworker from '../worker/gradleworker';
import { useSelector, useDispatch } from 'react-redux'
import { makeStyles } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import MaterialTable from "material-table"
import { setIsLoading, setGradleProjects } from '../actionTypes';
import DependencyDetailPanel from './DependencyDetailPanel';

const useStyles = makeStyles(theme => ({
    list: {
        paddingBottom: 50,
    },
}))

function GradleManager() {
    const classes = useStyles();
    const path = useSelector(state => state.directory);
    const projects = Object.values(useSelector(state => state.gradle));

    const dispatch = useDispatch();

    function refreshGradleProjects(dir) {
        dispatch(setIsLoading(true));
        return gradleworker.refreshGradleProjects(dir).then(result => {
            let gradleProjects = {};
            result.forEach(item => {
                gradleProjects[item.key] = item;
            })
            dispatch(setGradleProjects(gradleProjects));
        })
    }

    let columns = [
        { title: "Project Name", field: "name" },
        { title: "Version", field: "version" }
    ]

    return (
        <Container className={classes.list}>
            <MaterialTable
                columns={columns}
                data={projects}
                title="Repos"
                onRowClick={(event, rowData, togglePanel) => togglePanel()}
                detailPanel={rowData => {
                    return (
                        <DependencyDetailPanel project={rowData} projects={projects} />
                    );
                }}
                options={{
                    paging: false,
                    actionsColumnIndex: columns.length,
                    selection: true,
                    maxBodyHeight: '500px',
                    headerStyle: { position: 'sticky', top: 0 },
                }}
                actions={[
                    {
                        icon: 'refresh',
                        tooltip: 'Refresh projects',
                        isFreeAction: true,
                        onClick: () => {
                            refreshGradleProjects(path).then(result => {
                                dispatch(setIsLoading(false));
                            })
                        }
                    }
                ]}
            />
        </Container>
    );
}

export default GradleManager;