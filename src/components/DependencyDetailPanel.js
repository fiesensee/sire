import React from 'react'
import MaterialTable from "material-table"

function DependencyDetailPanel(props) {
    let columns = [
        { title: "Dependency Name", field: "name" },
        { title: "Version", field: "version" }
    ]

    function enhanceDeps(project, projects) {
        return project.deps.reduce((enhancedDeps, dep) => {
            console.log(enhancedDeps);
            let depProject = projects.filter(item => item.name === dep.name);
            if (depProject.length && depProject[0].version !== dep.version) {
                enhancedDeps.push({
                    "name": dep.name,
                    "version": dep.version + "(!=" + depProject[0].version + ")"
                });
            } else if (depProject.length !== 0) {
                enhancedDeps.push(dep);
            }
            return enhancedDeps;
        }, []);
    }

    const deps = enhanceDeps(props.project, props.projects);

    return (
        <div>
            <MaterialTable
                columns={columns}
                data={deps}
                options={{
                    search: false,
                    header: false,
                    toolbar: false,
                    showTitle: false,
                    paging: false
                }}
            />
        </div>
    );
}

export default DependencyDetailPanel;