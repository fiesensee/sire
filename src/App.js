import React, { useState } from 'react';
import './App.css';
import RepoTable from './components/RepoTable';
import TabPanel from './TabPanel';
import { Container, AppBar, makeStyles, Toolbar, LinearProgress, Typography, IconButton, Tabs, Tab } from '@material-ui/core'
import FolderOpenIcon from '@material-ui/icons/FolderOpen';
import { useSelector, useDispatch } from 'react-redux';
import { setDirectory } from './actionTypes';
import GradleManager from './components/GradleManager';

const useStyles = makeStyles(theme => ({
  appBar: {
    top: 'auto',
    bottom: 0,
  },
  grow: {
    flexGrow: 1,
  },
  hidden: {
    visibility: "hidden",
  },
}));



function App() {
  const classes = useStyles();

  const isLoading = useSelector(state => state.isLoading);
  const directory = useSelector(state => state.directory);
  const [tab, setTab] = useState(0);
  const dispatch = useDispatch();

  const handleChange = (_event, newValue) => {
    setTab(newValue);
  }

  function getDirectory() {
    let dir = window.electron.dialog.showOpenDialogSync({ properties: ['openDirectory'] });
    console.log(dir);
    if (dir !== undefined) {
      return dir[0];
    } else {
      return directory;
    }
  }

  return (
    <div className="App">
      <AppBar position="static" color="inherit">
        <Tabs value={tab} onChange={handleChange}>
          <Tab label="Git" id='simple-tab-0' />
          <Tab label="Gradle" id='simple-tab-1' />
        </Tabs>
      </AppBar>
      <LinearProgress className={isLoading ? null : classes.hidden} color="secondary" />
      <Container>
        <TabPanel value={tab} index={0}>
          <RepoTable />
        </TabPanel>
        <TabPanel value={tab} index={1}>
          <GradleManager />
        </TabPanel>
        <AppBar position="fixed" color="inherit" className={classes.appBar}>
          <Toolbar>
            <IconButton onClick={() => dispatch(setDirectory(getDirectory()))}>
              <FolderOpenIcon></FolderOpenIcon>
            </IconButton>
            <Typography>{directory}</Typography>
            <div className={classes.grow} />
          </Toolbar>
        </AppBar>
      </Container>
    </div>
  );
}

export default App;
