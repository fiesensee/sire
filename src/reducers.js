import { combineReducers } from 'redux';
import {
    SET_GIT_REPO,
    SET_GIT_REPOS,
    SET_GRADLE_PROJECTS,
    SET_IS_LOADING,
    SET_DIRECTORY
} from './actionTypes';

function isLoading(state = false, action) {
    switch (action.type) {
        case SET_IS_LOADING:
            return action.isLoading;
        default:
            return state;
    }
}

function git(state = {}, action) {
    switch (action.type) {
        case SET_GIT_REPOS:
            return action.repos;
        case SET_GIT_REPO:
            return {
                ...state,
                [action.repo.key]: action.repo
            }
        default:
            return state;
    }
}

function gradle(state = {}, action) {
    switch (action.type) {
        case SET_GRADLE_PROJECTS:
            return action.projects;
        default:
            return state;
    }
}

function directory(state = "", action) {
    switch (action.type) {
        case SET_DIRECTORY:
            return action.directory;
        default:
            return state;
    }
}

const rootReducer = combineReducers({
    isLoading,
    git,
    gradle,
    directory
})

export default rootReducer;