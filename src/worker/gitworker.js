import path from 'path';
import os from 'os';
import { readDir } from './fileworker';
var nodeGit = window.require('nodegit')

var sshPublicKeyPath = path.join(os.homedir(), ".ssh/id_rsa.pub");
var sshPrivateKeyPath = path.join(os.homedir(), ".ssh/id_rsa");

var fetchOptions = {}
fetchOptions.fetchOpts = {
    callbacks: {
        certificateCheck: function () { return 0; },
        credentials: function (url, userName) {
            return nodeGit.Cred.sshKeyNew(
                userName,
                sshPublicKeyPath,
                sshPrivateKeyPath,
                "");
        }
    }
};

function refreshRepo(dir) {
    console.log("refreshing repo " + dir)
    // return repo.fetch("origin", fetchOptions.fetchOpts);
    return nodeGit.Repository.open(dir).then(repo => {
        let repoData = {
            repo: repo,
            repoName: path.basename(dir),
            changes: [],
            path: dir
        }
        return repo.fetch("origin", fetchOptions.fetchOpts).then(() => {
            return getRepoStatus(repoData).then(statuses => {
                repoData.changes = statuses;
                // return repoData;
            }).then(() => {
                return repo.getCurrentBranch().then(ref => {
                    repoData.currentBranch = ref.name();
                    return repoData;
                })
            }).then((repoData) => {
                let localCommits = []
                let remoteCommits = []
                // repo.getReferenceNames(nodeGit.Reference.TYPE.ALL).then(refs => console.log(refs))
                return repo.getHeadCommit().then(commit => {
                    return getHistory(commit, localCommits);
                }).then(() => repo.getReferenceCommit("refs/remotes/origin/" + repoData.currentBranch
                    .replace("refs/heads/", "")).then(commit => {
                    return getHistory(commit, remoteCommits);
                })).then(() => {
                    let commitsAhead = localCommits.filter(commit => !remoteCommits.includes(commit));
                    let commitsBehind = remoteCommits.filter(commit => !localCommits.includes(commit));
                    repoData.commitsAhead = commitsAhead.length;
                    repoData.commitsBehind = commitsBehind.length;
                    console.log("finished refresing " + dir);
                    return repoData;
                })
            })
        }).catch(err => {
            console.warn(err);
            return undefined;
        });
    }).catch(err => {
        console.warn(err);
    });
}

function pullRepo(dir) {
    console.log("pulling repo " + dir)
    return nodeGit.Repository.open(dir).then(async repo => {
        let currentBranch = await repo.getCurrentBranch().then((ref) => ref.name());
        currentBranch = currentBranch.replace("refs/heads/", "");
        return repo.mergeBranches(currentBranch, "origin/" + currentBranch).then(oid => {
            console.log("succesfully pulled");
            console.log(oid);
        }).then(() => refreshRepo(dir));
    }).catch((err) => {
        console.warn(err);
    });
}

function pushRepo(dir) {
    console.log("pushing repo " + dir)
    return nodeGit.Repository.open(dir).then(async repo => {
        let currentBranch = await repo.getCurrentBranch().then((ref) => ref.name());
        return repo.getRemote("origin").then(remote => {
            return remote.push(currentBranch, fetchOptions.fetchOpts).then((code) => {
                console.log("successfully pushed");
                console.log(code);
            }).then(() => refreshRepo(dir));
        });
    }).catch((err) => {
        console.warn(err);
    });
}

function refreshRepos(baseDir) {
    var basePath = path.resolve(baseDir);

    return readDir(basePath).then(dirs => {
        return dirs.map(dir => {
            return refreshRepo(dir);
        })
    });
}


function getHistory(commit, commits) {
    return new Promise((resolve, reject) => {
        let history = commit.history();
        history.on('commit', commit => {
            commits.push(commit.sha());
        });
        history.on('end', () => resolve());
        history.start();
    })
}

function getRepoStatus(repoData) {
    return repoData.repo.getStatus().then(statuses => {
        function statusToText(status) {
            var words = [];
            if (status.isNew()) { words.push("NEW"); }
            if (status.isModified()) { words.push("MODIFIED"); }
            if (status.isTypechange()) { words.push("TYPECHANGE"); }
            if (status.isRenamed()) { words.push("RENAMED"); }
            if (status.isIgnored()) { words.push("IGNORED"); }

            return words.join(" ");
        }

        return statuses.map(file => {
            let changeText = file.path() + " " + statusToText(file)
            // console.log(changeText);
            return (changeText);
        });
    });
}

export { refreshRepos, refreshRepo, pullRepo, pushRepo };

