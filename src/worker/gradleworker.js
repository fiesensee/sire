import path from 'path';
import fs from 'fs';
import { readDir } from './fileworker';

function refreshGradleProjects(dir) {
    return readDir(dir).then(dirs => {
        return Promise.all(dirs.map(dir => {
            return refreshGradleProject(dir);
        }));
    });
}

function refreshGradleProject(dir) {
    return Promise.all([getFileContents(path.join(dir, "build.gradle")),
    getFileContents(path.join(dir, "settings.gradle")),
    getFileContents(path.join(dir, "src", "main", "resources", "version.properties"))])
        .then(([buildFile, settingsFile, versionSettingsFile]) => {
            let version = determineVersion(buildFile, versionSettingsFile);
            let deps = getDependencies(buildFile);
            let name = determineProjectName(buildFile, settingsFile);
            // console.log(dir + " " + name + " " + version);
            return {
                "key": path.basename(dir),
                "name": name === undefined ? path.basename(dir) : name,
                "version": version,
                "deps": deps
            }
        })
}

function determineVersion(buildFile, versionSettingsFile) {
    let matchInfo;
    if (versionSettingsFile !== undefined) {
        let re = /version=(.*)/g;
        matchInfo = re.exec(versionSettingsFile);
    }
    if (matchInfo === null && buildFile !== undefined) {
        let re = /(?:project\.)?version\W*=\W*([0-9a-zA-z.-]+)/g;
        matchInfo = re.exec(buildFile);
    }
    if (matchInfo !== null) {
        return matchInfo[1];
    } else {
        return undefined;
    }
}

function determineProjectName(buildFile, settingsFile) {
    let matchInfo;
    if (settingsFile !== undefined) {
        let re = /rootProject\.name\W*=[\W']*([0-9a-zA-Z-_]*)[\W']*/g
        matchInfo = re.exec(settingsFile);
    }
    // TODO handle buildfile basename
    if (matchInfo !== null) {
        return matchInfo[1];
    } else {
        return undefined;
    }
}

function getDependencies(buildFile) {

    let dependencies = [];
    let stringNotationRe = /['"]([\w\d.\-^]+):([\w\d.\-^]+):([\w\d.\-^]+)['"]/;
    let splitUpNotationRe = /group: ["']([\w\d.\-^]+)['"], name: ["']([\w\d.\-^]+)['"], version: ["']([\w\d.\-^]+)['"]/;

    let depLines = buildFile.split("\n")
    depLines.forEach(line => {
        // ignore comments
        if (!line.trim().startsWith("//")) {
            // relevant dependency line
            if (line.trim().startsWith("compile")) {
                // handle split up notation
                let depMatchInfo = undefined
                if (line.includes("group:")) {
                    depMatchInfo = splitUpNotationRe.exec(line);
                } else {
                    // handle string notation
                    depMatchInfo = stringNotationRe.exec(line);
                }
                if (depMatchInfo !== null) {
                    dependencies.push({
                        "group": depMatchInfo[1],
                        "name": depMatchInfo[2],
                        "version": depMatchInfo[3]
                    })
                } else {
                    console.log("could not determine dep for: " + line);
                }
            }
        }
    })

    return dependencies;
}

async function getFileContents(file) {
    return new Promise((resolve, reject) => {
        fs.readFile(file, (err, data) => {
            if (err === null) {
                resolve(data.toString());
            }
            resolve("");
        })
    })
}

export { refreshGradleProjects }