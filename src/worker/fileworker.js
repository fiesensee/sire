import path from 'path';
import fs from 'fs';

export function readDir(basePath) {
    return new Promise((resolve, reject) => {
        fs.readdir(basePath, async (err, files) => {
            if (err) {
                console.error(err);
                reject(err);
            }
            const data = await Promise.all(files.map(file => {
                if (err) {
                    console.error(err);
                    // reject(err);
                }
                let repoPath = path.join(basePath, file);
                // console.log(repoPath);
                return new Promise((resolve, reject) => {
                    fs.stat(repoPath, (err, stat) => {
                        if (err) {
                            console.error(err);
                            reject(err);
                        }
                        if (stat.isDirectory()) {
                            // directoryPaths.push(path.join(basePath, file));
                            resolve(path.join(basePath, file));
                        }
                        else {
                            reject(file + " is not a dir");
                        }
                    });
                }).then(data => {
                    return data;
                }).catch(err => {
                    console.warn(err);
                });
            }));
            // console.log(data.filter(path => path !== undefined));
            resolve(data.filter(path => path !== undefined));
        });
    });
}
