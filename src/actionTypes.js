export const SET_IS_LOADING = 'SET_IS_LOADING'
export const SET_DIRECTORY = 'SET_DIRECTORY'
export const SET_GIT_REPO = 'SET_GIT_REPO'
export const SET_GIT_REPOS = 'SET_GIT_REPOS'
export const SET_GRADLE_PROJECTS = 'SET_GRADLE_PROJECTS'


export function setRepo(repo) {
    return { type: SET_GIT_REPO, repo }
}

export function setRepos(repos) {
    return { type: SET_GIT_REPOS, repos }
}

export function setIsLoading(isLoading) {
    return { type: SET_IS_LOADING, isLoading }
}

export function setDirectory(directory) {
    return { type: SET_DIRECTORY, directory }
}

export function setGradleProjects(projects) {
    return { type: SET_GRADLE_PROJECTS, projects }
}