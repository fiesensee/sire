import React, { useEffect } from 'react';
import App from './App';
import { Provider } from 'react-redux';
import configureStore from './configureStore';
import { PersistGate } from 'redux-persist/integration/react';

const { store, persistor } = configureStore();

function AppContainer() {

    useEffect(() => {
        const log = require("electron-log")
        log.transports.file.level = "debug"
        window.autoUpdater.logger = log
        window.autoUpdater.checkForUpdatesAndNotify();
    });

    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <App />
            </PersistGate>
        </Provider>
    );
}

export default AppContainer;